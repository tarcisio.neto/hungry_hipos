import unittest
from app.game import Game

class TestGameHipos(unittest.TestCase):
    def test_se_jogo_retorna_numero_de_pulos_corretos_01(self):
        board = [[1, 1, 0, 0, 0],
                 [1, 1, 0, 0, 0],
                 [0, 0, 0, 0, 0],
                 [0, 0, 0, 1, 1],
                 [0, 0, 0, 1, 1]]
        game = Game(board,5)
        self.assertEqual(game.play(), 2)

    def test_se_jogo_retorna_numero_de_pulos_corretos_02(self):
        board = [[1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1],
                 [1, 1, 1, 0, 0],
                 [0, 0, 0, 1, 1],
                 [0, 0, 0, 1, 1]]
        game = Game(board,5)
        self.assertEqual(game.play(), 3)

    def test_se_jogo_retorna_numero_de_pulos_corretos_03(self):
        board = [[1, 0, 1, 0, 1],
                [0, 1, 0, 1, 0],
                [1, 0, 1, 0, 1],
                [0, 1, 0, 1, 0],
                [1, 0, 1, 0, 1]]
        game = Game(board,5)
        self.assertEqual(game.play(), 13)


    def test_se_jogo_retorna_numero_de_pulos_corretos_04(self):
        board = [[1, 1, 0, 0, 1],
                 [0, 0, 0, 1, 0],
                 [1, 1, 0, 0, 1],
                 [1, 0, 1, 1, 0],
                 [0, 0, 1, 0, 1]]
        game = Game(board, 5)
        self.assertEqual(game.play(), 7)


    def test_se_jogo_retorna_numero_de_pulos_corretos_com_matriz_menor_que_05(self):
        board = [[1, 1, 1, 1],
                 [1, 1, 0, 1],
                 [1, 0, 1, 1],
                 [1, 1, 1, 1]]
        game = Game(board, 4)
        self.assertEqual(game.play(), 1)

    def test_se_jogo_retorna_numero_de_pulos_corretos_com_matriz_maior_que_05(self):
        board = [[1, 0, 0, 0, 0, 1],
                 [0, 0, 1, 1, 0, 0],
                 [1, 0, 1, 0, 1, 1],
                 [0, 0, 1, 0, 0, 1],
                 [1, 0, 1, 1, 0, 0],
                 [1, 0, 1, 0, 1, 0]]
        game = Game(board, 6)
        self.assertEqual(game.play(), 7)



class Game:
   def __init__(self, board: list, n: int):
      self.board = board
      self.n = n

# Inicia o jogo chamando a primeira jogada, verifica se o jogo acabou ou não e retorna o número
   def play(self) -> int:
      self.count_play = 0
      for x in range(0, self.n):
         for y in range(0, self.n):
            if self.board[x][y] == 1:
               self.start_play(x, y)
               if self.check_end_play(self.board, self.n) == 0:
                  break
      return self.count_play

# Contabiliza o número de jogadas e chama o método para seguir com a jogada.
   def start_play(self, x, y):
      self.count_play += 1
      self.follow_play(x, y)

# Da sequencia na jogada iniciada e chama a  verificação de  quantas posições serão pegas
   def follow_play(self,x,y):
      self.board[x][y] = 0
      self.check_follow_play(x, y)

# Método que faz a checagem se os vizinhos da posição inicial podem fazer parte da jogada.
   def check_follow_play(self, x, y):
      self.follow_play(x, y - 1) if y - 1 >= 0 and self.board[x][y - 1] == 1 else None
      self.follow_play(x - 1, y) if x - 1 >= 0 and self.board[x - 1][y] == 1 else None
      self.follow_play(x + 1, y) if x + 1 < self.n and self.board[x + 1][y] == 1 else None
      self.follow_play(x, y + 1) if y + 1 < self.n and self.board[x][y + 1] == 1 else None

# Método que faz a checagem se o tabuleiro (matriz) esta vazio para o jogo ser encerrado.
   def check_end_play(self,matriz,n):
      lines = n
      column = n
      sum_values_board = 0
      for l in range(0, lines):
         for c in range(0, column):
            sum_values_board += matriz[l][c]
      return sum_values_board





from app.game import Game

class Startup:

    def execute(self) -> None:
        board = [[1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1],
                 [1, 1, 1, 0, 0],
                 [0, 0, 0, 1, 1],
                 [0, 0, 0, 1, 1]]
        game = Game(board, 5)

# chama o jogo para ser executado e retorna (print) na tela quantidade de pulos do hipopótamo
        print(game.play())
